package nz.ac.vuw.mcs.fridge.gui;

import javax.swing.JOptionPane;

import nz.ac.vuw.mcs.fridge.backend.DBWorker;
import nz.ac.vuw.mcs.fridge.backend.InterfridgeException;

/**
 * Uses JOptionPane-based dialogs to prompt the user through crediting their account by a positive amount.
 * @author neil
 */
public class CreditAccountDialog {
	public CreditAccountDialog() {
		while (true) {
			String creditAmountString = JOptionPane.showInputDialog(FridgeClient.mainWindow, "How much would you like to credit your account by? (eg. 5.00)", "Enter amount to credit account by", JOptionPane.OK_CANCEL_OPTION | JOptionPane.QUESTION_MESSAGE);
			if (creditAmountString == null) {
				// they pushed cancel, stop here
				return;
			}
			// attempt to convert the number into a double
			double creditAmount;
			try {
				creditAmount = Double.parseDouble(creditAmountString);
				// do the credit
				DBWorker.creditCurrentUserAccount(creditAmount);
				// update the balance label
				FridgeClient.mainWindow.currentOrderPanel.userStats.updateUserBalance();

				JOptionPane.showMessageDialog(FridgeClient.mainWindow, "Account credited successfully\nDon't forget to put the money in the drawer!", "Account credited", JOptionPane.INFORMATION_MESSAGE);
				return;
			}
			catch (NumberFormatException e) {
				JOptionPane.showMessageDialog(FridgeClient.mainWindow, "That number wasn't valid. Don't enter the $ or anything that's not a digit or a '.'\nPlease try again.", "Invalid number", JOptionPane.ERROR_MESSAGE);
			}
			catch (InterfridgeException e) {
				JOptionPane.showMessageDialog(FridgeClient.mainWindow, e.getMessage(), "Error crediting", JOptionPane.ERROR_MESSAGE);
			}
		}
	}
}
