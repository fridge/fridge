package nz.ac.vuw.mcs.fridge.gui;

import java.awt.AWTKeyStroke;
import java.awt.Color;
import java.awt.GridBagLayout;
import java.awt.KeyboardFocusManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.util.HashSet;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

import nz.ac.vuw.mcs.fridge.backend.DBWorker;
import nz.ac.vuw.mcs.fridge.gui.util.GridBagHelper;

/**
 * This class extends JPanel to render the contents and functions of the top panel
 * inside the selection pane, giving the users the ability to add a code and quantity.
 * <p>
 * It should be added to the topPanel object by WindowHandler.
 * <p>
 * It will do sanity checking on the product code and quantity fields before submitting a new
 * order row.
 */
public class AddItemToOrderPanel extends JPanel {
	private static final long serialVersionUID = 1L;
	private static final JTextField txtProductCode = new JTextField(6);
	private static final JTextField txtQuantity = new JTextField(4);
	private static final JButton btnSubmit = new JButton();
	
	/**
	 * This constructor draws all the elements and sets their action handlers.
	 *
	 */
	public AddItemToOrderPanel() {
		// set our layout to gridbag
		this.setLayout(new GridBagLayout());
		
		// set our border
		TitledBorder borderWithTitle = BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED), "Add item to order");
		this.setBorder(borderWithTitle);
		
		// setup the labels
		JLabel label = new JLabel("Product");
		this.add(label, GridBagHelper.getConstraintWithInsets(1, 0, 1, 1, "centre", 2, 30, 2, 10));
		
		label = new JLabel("Quantity");
		this.add(label, GridBagHelper.getConstraintWithInsets(2, 0, 1, 1, "centre", 2, 10, 2, 30));
		
		// set default quantity to 1
		txtQuantity.setText("1");
		
		// set losing focus listener on product code (don't let them leave if the code is invalid)
		txtProductCode.addFocusListener(new FocusListener() {
			public void focusLost(FocusEvent e) {
				// losing focus, set code to uppercase and check code
				
				txtProductCode.setText(txtProductCode.getText().toUpperCase());
				// is it blank? (blank = submit order)
				if (txtProductCode.getText().equals("")) {
					return; // do nothing, action handler will handle this
				}
				
				// is it valid? this will stop them in their tracks if it isn't
				checkProductBox(false); // false means don't textsearch
				
				// otherwise let em go
			}
			public void focusGained(FocusEvent e) {
				// select the text in this box
				txtProductCode.selectAll();
			}
		});
		
		// set losing focus listener on quantity to select the text when they tab in
		txtQuantity.addFocusListener(new FocusListener() {
			public void focusLost(FocusEvent e) {
				// do nothing
			}
			public void focusGained(FocusEvent e) {
				// select the text in this box
				txtQuantity.selectAll();
			}
		});
		
		// set the focus change keys
		HashSet<AWTKeyStroke> traversalKeys = new HashSet<AWTKeyStroke>();
		traversalKeys.add(AWTKeyStroke.getAWTKeyStroke(KeyEvent.VK_TAB, 0, false));
		traversalKeys.add(AWTKeyStroke.getAWTKeyStroke(KeyEvent.VK_RIGHT, 0, false)); // right arrow key
		txtProductCode.setFocusTraversalKeys(KeyboardFocusManager.FORWARD_TRAVERSAL_KEYS, traversalKeys);
		
		traversalKeys = new HashSet<AWTKeyStroke>();
		traversalKeys.add(AWTKeyStroke.getAWTKeyStroke(KeyEvent.VK_TAB, InputEvent.SHIFT_DOWN_MASK, false)); // shift-tab
		traversalKeys.add(AWTKeyStroke.getAWTKeyStroke(KeyEvent.VK_LEFT, 0, false)); // left arrow key
		txtQuantity.setFocusTraversalKeys(KeyboardFocusManager.BACKWARD_TRAVERSAL_KEYS, traversalKeys);
		
		txtProductCode.registerKeyboardAction(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// user pressed down arrow, move focus to table
				FridgeClient.mainWindow.currentOrder.getTable().requestFocus();
			}
		}, KeyStroke.getKeyStroke(KeyEvent.VK_DOWN, 0), JComponent.WHEN_FOCUSED);
		
		txtQuantity.registerKeyboardAction(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// user pressed down arrow, move focus to table
				FridgeClient.mainWindow.currentOrder.getTable().requestFocus();
			}
		}, KeyStroke.getKeyStroke(KeyEvent.VK_DOWN, 0), JComponent.WHEN_FOCUSED);
		
		// setup the fields
		this.add(txtProductCode, GridBagHelper.getConstraintWithInsets(1, 1, 1, 1, "centre", 0, 30, 10, 10));
		this.add(txtQuantity, GridBagHelper.getConstraintWithInsets(2, 1, 1, 1, "centre", 0, 10, 10, 30));
		
		// set the action listeners for the boxes to either add the product to the order if there's a code in the box
		// or submit the order if the product code box is empty
		txtProductCode.registerKeyboardAction(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// they pushed enter. is the field blank or the work FINALISE (for barcode to submit)
				if (txtProductCode.getText().length() == 0) {
					// submit the order now with confirm
					FridgeClient.mainWindow.currentOrder.submitOrder(true);
				}
				else if (txtProductCode.getText().equals("FINALISE")) {
					// submit the order now without confirm
					FridgeClient.mainWindow.currentOrder.submitOrder(false);
				}
				else {
					addCurrentLineToOrderIfOK();
				}
			}
		}, KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0), JComponent.WHEN_FOCUSED);
		
		txtQuantity.registerKeyboardAction(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				addCurrentLineToOrderIfOK();
			}
		}, KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0), JComponent.WHEN_FOCUSED);
		
		// add buttons to submit and cancel the order
		btnSubmit.setText("<html><font size=5>Submit Order&nbsp;&nbsp;&raquo;</font></html>");
		JButton btnCancel = new JButton("<html><font size=5>&laquo;&nbsp;&nbsp;Cancel Order</font></html>");
		
		btnSubmit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// Check for text in the product code box, refuse to submit if there is
				// Added by Neil 5/8/05
				if (txtProductCode.getText().length() > 0) {
					// Display a warning dialog
					JOptionPane.showMessageDialog(FridgeClient.mainWindow, "You tried to submit an order while there was still text in the product code box. Please clear it out or press enter to add it to the order before attempting to submit.", "Unprocessed code in product code box", JOptionPane.ERROR_MESSAGE);
					return;
				}
				
				// submit this order 
				FridgeClient.mainWindow.currentOrder.submitOrder(true);
			}
		});
		
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// Do NOT submit this order 
				FridgeClient.mainWindow.resetToLogin();
			}
		});
		
		// disallow keyboard focus on the buttons (we don't want that, they can submit
		// with just the keyboard)
		btnSubmit.setFocusable(false);
		btnCancel.setFocusable(false);
		
		this.add(btnCancel, GridBagHelper.getConstraintWithInsets(0, 0, 1, 2, "left", 0, 0, 0, 100));
		this.add(btnSubmit, GridBagHelper.getConstraintWithInsets(3, 0, 1, 2, "right", 0, 100, 0, 0));
	}
	
	/**
	 * This method is intended to be called from outside the class to set the focus into the product code.
	 * It should be called when a product has finished being added, or a special keyboard action (up-arrow maybe) is
	 * pressed and focus is desired back in product code
	 *
	 */
	public void setFocusOnProductCode() {
		txtProductCode.requestFocus();
	}
	
	public void setProductCodeText(String text) {
		txtProductCode.setText(text);
	}
	
	/**
	 * This private method is called when the user presses enter from either the product code or
	 * quantity boxes to add the item to the order.
	 * <p>
	 * It will set the product box to uppercase before checking, as all codes are uppercase.
	 * <p>
	 * Barcodes are matched too, which is why this function (and validateProductCode() return the
	 * code rather than a boolean)
	 * <p>
	 * If the code isn't OK, it performs a text search on descriptions to see if it can offer
	 * some close matches. If not, the text search class will call setInvalid(), which puts the cursor back
	 * in the code box with the text "INVALID" in it
	 * 
	 * @return the product if the (bar)code in the box exists and is enabled
	 */
	private String checkProductBox(boolean doSearchIfInvalid) {
		String originalEntry = txtProductCode.getText();
		txtProductCode.setText(txtProductCode.getText().toUpperCase());
		String validatedProductCode = DBWorker.validateProductCode(txtProductCode.getText());
		if (validatedProductCode == null) {
			if (doSearchIfInvalid) {
				setProductCodeText(""); // prevents focus lost retriggering this check
				new ProductTextSearch(originalEntry);
			}
			else setInvalid();
			return null; // cancel for now, if user picks a match, it'll paste it back into the product code box
		}
		// else if code they entered was ok, just return it:
		return validatedProductCode;
	}
	
	/**
	 * This method is called by the action listeners that add things to the order within this panel.
	 * It will check the box using the checkProductBox() function, and if it's ok, will create a new
	 * OrderLine and add it to the current order.
	 * 
	 * It can also be called from the textsearch box when a user makes a selection
	 *
	 */
	public void addCurrentLineToOrderIfOK() {
		// checks the current line, then adds it
		String validatedProductCode = checkProductBox(true);
		if (validatedProductCode != null) {
			// it's ok. check if quantity is blank, if so, make it 1
			if (txtQuantity.getText().length() == 0) {
				txtQuantity.setText("1");
			}
			
			// check if quantity is a number
			int qty;
			try {
				qty = Integer.parseInt(txtQuantity.getText());
			}
			catch (NumberFormatException e) {
				// select the qty field again to show them the error of their ways
				txtQuantity.requestFocus();
				txtQuantity.selectAll();
				return;
			}
			FridgeClient.mainWindow.currentOrder.addItem(validatedProductCode, qty);
			
			// clear form an return focus
			txtProductCode.setText("");
			txtQuantity.setText("1");
			txtProductCode.requestFocus();
		}
	}

	/**
	 * Sets text in product code to INVALID and gives it focus. Called when there's no result for a textsearch
	 */
	public void setInvalid() {
		txtProductCode.requestFocus();
		txtProductCode.setText("INVALID");
		txtProductCode.selectAll();
	}

	/**
	 * This method will reset the form to its inital state in case the last user left it with an invalid product
	 *called by windowHandler when swapping the window panes
	 *
	 *It now also sets the submit button disabled (to be enabled when an orderline is added to the order)
	 */
	public void clearFields() {
		txtProductCode.setText("");
		txtQuantity.setText("1");
		
		// Set the submit button disabled until the first product is added to the order
		// This feature requested by Philip, implemented by Neil 5/8/05
		btnSubmit.setEnabled(false);
		btnSubmit.setForeground(new Color(102, 102, 102));
	}
	
	/**
	 * Enable the submit button on this panel after someone has added an item to 
	 * the order. This is called by the addItemToOrder() method in the Order object.
	 */
	public void enableSubmitButton() {
		if(!btnSubmit.isEnabled()) {
			btnSubmit.setEnabled(true);
			btnSubmit.setForeground(new Color(0, 0, 0));
		}
	}
}
