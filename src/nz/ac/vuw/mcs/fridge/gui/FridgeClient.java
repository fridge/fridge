package nz.ac.vuw.mcs.fridge.gui;

import java.awt.Component;
import java.sql.SQLException;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 * The main program class for FridgeClient. The program invokes everything on the event-dispatch
 * thread, so not much is done here besides instanciating the main window and displaying it.
 * @author neil
 */
public class FridgeClient {
	static WindowHandler mainWindow;
	static final String version = "1.09";
	static final long startTime = System.currentTimeMillis();

	/**
	 * @param args are command-line options. ignored.
	 */
	public static void main(String[] args) {
		mainWindow = new WindowHandler();
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				mainWindow.displayWindowWithLoginPanel();
			}
		});
	}

	/**
	 * Prints out a database error message using the main window is a parent. Lists the exception that occured.
	 * @param message is the custom message about what went wrong
	 * @param sqlEx is the SQLException that was caught in the calling function.
	 * <p>
	 * This function is FATAL, ie the program quits after displaying the message.
	 * 
	 * In application, the fridgeclient should be placed in a while (java... > 0); do true; done; loop.
	 * This will bounce it back up after an exception occurs. Hopefully we won't get any exceptions due to
	 * programming errors :D
	 */
	public static void showError(String message, SQLException sqlEx) {
		// uses FridgeClient.mainWindow as the parent window
		if (FridgeClient.mainWindow != null) {
			showError(message, "Fatal database error", sqlEx, mainWindow, true);
		}
		else {
			// no window present. make an invisible parent
			showError(message, "Fatal database error", sqlEx, new JFrame(), true);
		}
	}

	// the below method can be called when more details are required eg. title and parent
	/**
	 * This is an extended version of showError() that allows other parent windows than main window, and allows the title
	 * to be set.
	 * <p>
	 * This function is optionally fatal, if the error is serious and the calling party specifies isFatal = true
	 */
	public static void showError(String message, String title, SQLException sqlEx, Component parent, boolean isFatal) {
		JOptionPane.showMessageDialog(parent, message + "\nException: " + sqlEx, title, JOptionPane.ERROR_MESSAGE);
		if (isFatal) {
			System.exit(1);
		}
	}

	public static void showError(String message, String title, SQLException sqlEx, boolean isFatal) {
		showError(message, title, sqlEx, mainWindow, isFatal);
	}
}
