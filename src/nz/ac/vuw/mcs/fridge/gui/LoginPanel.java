package nz.ac.vuw.mcs.fridge.gui;

import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

import nz.ac.vuw.mcs.fridge.backend.DBWorker;
import nz.ac.vuw.mcs.fridge.backend.InterfridgeException;
import nz.ac.vuw.mcs.fridge.gui.util.GridBagHelper;

/**
 * Extends JPanel to provide a panel to add to topPanel that requests and checks a login from the user
 * @author neil
 *
 */
public class LoginPanel extends JPanel {

	private static final long serialVersionUID = 1L; //wtf?
	
	JLabel lblLoginStatus = new JLabel();
	final JTextField txtUsername = new JTextField(10);
	final JPasswordField txtPassword = new JPasswordField(10);
	JLabel lblServed = new JLabel();
	
	// constructs the login panel, setting event handlers etc
	
	public LoginPanel() {
		// set layout to gridbaglayout
		this.setLayout(new GridBagLayout());
		
		// Add the "Welcome to fride" banner
		JLabel lblWelcomeBanner = new JLabel("<html><font size=18>Welcome to fridge</font></html>");
		this.add(lblWelcomeBanner, GridBagHelper.getConstraint(0, 0, 2, 1, "centre"));
		
		lblServed.setText("<html><b>You shouldn't see this</b></html>");
		this.add(lblServed, GridBagHelper.getConstraint(0, 1, 2, 1, "centre"));
		
		// Add the "please login below" label
		JLabel lblPleaseLoginBelow = new JLabel("Please login below");
		this.add(lblPleaseLoginBelow, GridBagHelper.getConstraintWithInsets(0, 2, 2, 1, "centre", 0, 0, 20, 0));
		
		// username label
		JLabel lblUsername = new JLabel("User");
		this.add(lblUsername, GridBagHelper.getConstraint(0, 3, 1, 1, "centre"));
		
		// password label
		JLabel lblPassword = new JLabel("Password");
		this.add(lblPassword, GridBagHelper.getConstraint(1, 3, 1, 1, "centre"));
		
		// username box
		/*HashSet traversalKeys = new HashSet();
		traversalKeys.add(AWTKeyStroke.getAWTKeyStroke(KeyEvent.VK_ENTER, 0, false));
		traversalKeys.add(AWTKeyStroke.getAWTKeyStroke(KeyEvent.VK_TAB, 0, false));
		txtUsername.setFocusTraversalKeys(KeyboardFocusManager.FORWARD_TRAVERSAL_KEYS, traversalKeys);*/
		
		// listen for enter key, check barcode if it is one, otherwise shift focus to password
		txtUsername.registerKeyboardAction(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//TODO: Implement this, or remove it completely
				//if (!DBWorker.authenticateUserWithBarcodeHash(txtUsername.getText())) {
					// is not a barcode or is not valid, move focus to password
					txtPassword.requestFocus();
				/*}
				else loginSucceeded();*/
			}
		}, KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0), JComponent.WHEN_FOCUSED);
		
		this.add(txtUsername, GridBagHelper.getConstraintWithInsets(0, 4, 1, 1, "centre", 0, 10, 0, 2));
		
		// password box
		txtPassword.setEchoChar('\u2022'); // bullet character
		txtPassword.registerKeyboardAction(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// when the user presses enter in the password box, check the username and password
				if (DBWorker.authoriseUser(txtUsername.getText(), new String(txtPassword.getPassword()))) {
					loginSucceeded();
				}
				else {
					// login failed. Display a message and return focus to username box
					lblLoginStatus.setText("<html><font color=red><b>Login refused: "+DBWorker.message()+"</b></font></html>");
					txtUsername.setText("");
					txtPassword.setText("");
					txtUsername.requestFocus();
				}
			}
		}, KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0), JComponent.WHEN_FOCUSED);
		this.add(txtPassword, GridBagHelper.getConstraintWithInsets(1, 4, 1, 1, "centre", 0, 2, 0, 10));

		// progress label (member var of LoginPanel)
		lblLoginStatus.setText("Enter username and password, then press enter");
		this.add(lblLoginStatus, GridBagHelper.getConstraintWithInsets(0, 5, 2, 1, "centre", 10, 0, 0, 0));
		
		// set a titled border around the login panel
		TitledBorder borderWithTitle = BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED), "Login");
		this.setBorder(borderWithTitle);
		
	}

	/**
	 * 
	 */
	private void loginSucceeded() {
		// login successful. reset the message in case they've had unsuccessful attempts, clear text boxes
		lblLoginStatus.setText("Enter username and password, then press enter");
		txtUsername.setText("");
		txtPassword.setText("");
		FridgeClient.mainWindow.switchToOrderMode();
	}
	
	public void updateServed() {
		// sets x served message from DB
		try {
			lblServed.setText("<html><b>" + DBWorker.getNumServed() + " served</b></html>");
		}
		catch (InterfridgeException e) {
			System.out.println("Non-fatal error: Couldn't get number served from database in LoginPanel.java\n" + e);
		}
	}
}
