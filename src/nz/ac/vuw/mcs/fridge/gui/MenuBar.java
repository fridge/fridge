package nz.ac.vuw.mcs.fridge.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

import nz.ac.vuw.mcs.fridge.backend.DBWorker;
import nz.ac.vuw.mcs.fridge.backend.InterfridgeException;
import nz.ac.vuw.mcs.fridge.backend.util.MoneyHelper;

/**
 * Creates a JMenuBar for the selection window with an Options menu
 * @author neil
 *
 */
public class MenuBar extends JMenuBar {
	private static final long serialVersionUID = 1L;

	public MenuBar() {
		// create the menubar
		JMenu menu = new JMenu("Options");
		menu.setMnemonic(KeyEvent.VK_O);
		
		JMenuItem menuItem = new JMenuItem("Credit Account");
		menuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new CreditAccountDialog();
			}
		});
		menuItem.setMnemonic(KeyEvent.VK_C);
		menu.add(menuItem);

		menuItem.setMnemonic(KeyEvent.VK_A);
		menu.add(menuItem);
		
		menuItem = new JMenuItem("Transfer Credit");
		menuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// show the transfer credit dialog
				new TransferCreditDialog();
			}
		});
		menuItem.setMnemonic(KeyEvent.VK_T);		
		menu.add(menuItem);
		
		menuItem = new JMenuItem("Change Password");
		menuItem.setMnemonic(KeyEvent.VK_P);
		menuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new ChangePasswordDialog();
			}
		});
		menu.add(menuItem);
		
		menuItem = new JMenuItem("Update Product List");
		menuItem.setMnemonic(KeyEvent.VK_U);
		menuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				FridgeClient.mainWindow.productListPanel.populatePanelFromDB();
				JOptionPane.showMessageDialog(FridgeClient.mainWindow, "The product list was successfully updated from the database.", "Product list updated", JOptionPane.INFORMATION_MESSAGE);
			}
		});
		menu.add(menuItem);
		
		menuItem = new JMenuItem("Quit FridgeClient");
		menuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// this requires that they're an admin user
				try {
					if (DBWorker.isCurrentUserAdmin()) {
						System.exit(0);
					}
					else {
						// tell them they can't do it
						JOptionPane.showMessageDialog(FridgeClient.mainWindow, "You're not logged in as an admin user, you can't quit FridgeClient.", "Access Denied", JOptionPane.ERROR_MESSAGE);
					}
				}
				catch (InterfridgeException ex) {
					JOptionPane.showMessageDialog(FridgeClient.mainWindow, "Error checking whether you are an admin: " + ex.getMessage(), "Fridge error", JOptionPane.ERROR_MESSAGE);
					ex.printStackTrace();
				}
			}
		});
		menuItem.setMnemonic(KeyEvent.VK_Q);
		menu.add(menuItem);
		
		this.add(menu);
		
		menu = new JMenu("Help");
		menu.setMnemonic(KeyEvent.VK_H);
		
		menuItem = new JMenuItem("About");
		menuItem.setMnemonic(KeyEvent.VK_A);
		menuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// show the about dialog with statistics				
				String message = "FridgeClient-2005 version " + FridgeClient.version + "\n\nBy Neil Bertram and contributors in 2005\nReport bugs to fridge@mcs.vuw.ac.nz\n\n";
				
				Runtime r = Runtime.getRuntime();
				
				// Get some stats
				int numProcessors = r.availableProcessors();
				double freeMemory = MoneyHelper.round2dp(r.freeMemory()/1024D/1024D);
				double maxMemory = MoneyHelper.round2dp(r.maxMemory() /1024D/1024D);
				double totalMemory = MoneyHelper.round2dp(r.totalMemory()/1024D/1024D);
				double usedMemory = MoneyHelper.round2dp(totalMemory - freeMemory);
				
				// Calculate the uptime of fridge
				long uptimeMillis = System.currentTimeMillis() - FridgeClient.startTime;
				String uptimeString;
				if (uptimeMillis > 86400000) {
					uptimeString = Long.toString((uptimeMillis / 86400000L)) + " days";
				}
				else if (uptimeMillis > 3600000) {
					uptimeString = Long.toString((uptimeMillis / 3600000L)) + " hours";
				}
				else if (uptimeMillis > 60000) {
					uptimeString = Long.toString((uptimeMillis / 60000L)) + " minutes";
				}
				else {
					uptimeString = Long.toString((uptimeMillis / 1000L)) + " seconds";
				}
				
				message += "Current uptime is " + uptimeString + "\n\n";
				
				message +="Runtime statistics\n";
				message +="~~~~~~~~~~~~\n";
				message +="Processors: " + numProcessors + "\n";
				message +="Free mem: " + freeMemory + "MB\n";
				message +="Used mem: " + usedMemory + "MB\n";
				message +="Allocation limit: " + maxMemory + "MB\n";
				message +="Currently allocated: " + totalMemory + "MB";
				
				JOptionPane.showMessageDialog(FridgeClient.mainWindow, message, "About FridgeClient", JOptionPane.INFORMATION_MESSAGE);
			}
		});
		
		menu.add(menuItem);
		
		this.add(menu);
	}
}
