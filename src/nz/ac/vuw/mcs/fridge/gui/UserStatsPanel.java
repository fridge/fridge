package nz.ac.vuw.mcs.fridge.gui;

import java.awt.GridBagLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;

import nz.ac.vuw.mcs.fridge.backend.DBWorker;
import nz.ac.vuw.mcs.fridge.backend.InterfridgeException;
import nz.ac.vuw.mcs.fridge.backend.util.MoneyHelper;
import nz.ac.vuw.mcs.fridge.gui.util.GridBagHelper;

public class UserStatsPanel extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final JLabel lblOrderTotal = new JLabel();
	private final JLabel lblUserBalance = new JLabel();
	private double currentUserBalance;
	private double orderAmount;
	
	private String truncateNicely(String name, int length){
		if(name.length() <= length){
			return name;
		}
		else{
			return name.substring(0,length-3)+"...";
		}
	}
	
	public UserStatsPanel() {
		// create titled border
		//TitledBorder borderWithTitle = BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED), "Info");
		//this.setBorder(borderWithTitle);
	
		// use gridbaglayout
		this.setLayout(new GridBagLayout());
		int ypos = 0;
		JLabel label = new JLabel("<html><b><u>Current User</u></b></html>");
		String realName = "(Error getting real name)";
		try {
			realName = DBWorker.getCurrentUserRealName();
		}
		catch (InterfridgeException e) {
			e.printStackTrace();
		}
		JLabel lblCurrentUser = new JLabel("<html><font size=4><b>" + truncateNicely(realName, 20) + "</b></font></html>");
		this.add(label, GridBagHelper.getConstraint(0, ypos++, 1, 1, "centre"));
		this.add(lblCurrentUser, GridBagHelper.getConstraint(0, ypos++, 1, 1, "centre"));
		this.add(new JLabel("<html><i><font size=2>(User type not implemented)</font></i></html>"),
				GridBagHelper.getConstraint(0,ypos++,1,1,"centre"));
		
		label = new JLabel("<html><b><u>Account Balance</u></b></html>");
		this.add(label, GridBagHelper.getConstraint(0, ypos++, 1, 1, "centre"));
		
		// display the user balance big
		currentUserBalance = DBWorker.getCurrentUserBalance();
		String htmlRed = "";
		if (currentUserBalance < 0) {
			htmlRed = " color=red";
		}
		lblUserBalance.setText("<html><font size=16"+htmlRed+">" + MoneyHelper.formatCurrency(currentUserBalance) + "</font></html>");
		this.add(lblUserBalance, GridBagHelper.getConstraint(0, ypos++, 1, 1, "centre"));
		
		label = new JLabel("<html><b><u>Order Total</u></b></html>");
		this.add(label, GridBagHelper.getConstraint(0, ypos++, 1, 1, "centre"));
		
		// show the current order total
		lblOrderTotal.setText("<html><font size=16>$0.00</font></html>");
		this.add(lblOrderTotal, GridBagHelper.getConstraint(0, ypos++, 1, 1, "centre"));
	}
	public void updateOrderAmount(double newAmount) {
		lblOrderTotal.setText("<html><font size=16>" + MoneyHelper.formatCurrency(newAmount) + "</font></html>");
		orderAmount = newAmount;
		double userRemainingBalance = currentUserBalance;// - orderAmount;
		
		// if their balance is less than 0, show it in red
		String htmlRed = "";
		if (userRemainingBalance < 0) {
			htmlRed = " color=red";
		}
		lblUserBalance.setText("<html><font size=16" + htmlRed + ">" + MoneyHelper.formatCurrency(userRemainingBalance) + "</font></html>");
	}
	public void updateUserBalance() {
		// something has happend that changed the user's balance (transfer credit or credit account)
		// re-query the database
		currentUserBalance = DBWorker.getCurrentUserBalance();
		
		// update the labels by calling updateOrderAmount with the current amount
		updateOrderAmount(orderAmount);
		
		FridgeClient.mainWindow.transactionFeedPanel.refresh();
	}
}
