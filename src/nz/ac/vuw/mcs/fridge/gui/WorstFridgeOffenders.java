package nz.ac.vuw.mcs.fridge.gui;

import java.awt.GridLayout;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

import nz.ac.vuw.mcs.fridge.backend.DBWorker;
import nz.ac.vuw.mcs.fridge.backend.InterfridgeException;

public class WorstFridgeOffenders extends JPanel {
	private List<String> transactions;
	
	private static final long serialVersionUID = 1L;

	public WorstFridgeOffenders() {
		// create titled border
		TitledBorder borderWithTitle = BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED), "Naughty People! Bad!");
		this.setBorder(borderWithTitle);

		this.setLayout(new GridLayout(5,1));

		populate();
	}

	public void refresh() {
		this.removeAll();
		populate();
	}

	private void populate() {
		try {
			transactions = DBWorker.getWorstOffenders();

			int i = 0;
			for (String name: transactions) {
				JLabel label = new JLabel(name);
				label.setHorizontalAlignment(SwingConstants.CENTER);
				this.add(label, i++);
				System.out.println(name);
			}
		}
		catch (InterfridgeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
