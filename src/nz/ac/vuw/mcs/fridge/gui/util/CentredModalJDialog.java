/*
 * Created on Apr 19, 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package nz.ac.vuw.mcs.fridge.gui.util;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.Point;

import javax.swing.JDialog;
import javax.swing.JFrame;

/**
 * A Dialog class that extends JDialog and can centre itself relative to the specified parent
 */
public class CentredModalJDialog extends JDialog {

	private static final long serialVersionUID = 1L;

	/**	centers the dialog within the parent container [1.1]
	 *	(put that in the Dialog class)
	 */
	
	public CentredModalJDialog(JFrame parent, String title) {
		super(parent, title, true); // set modal
	}
	
	public void centreParent () {
		int x;
		int y;
		
		// Find out our parent 
		Container myParent = getParent();
		Point topLeft = myParent.getLocationOnScreen();
		Dimension parentSize = myParent.getSize();
		
		Dimension mySize = getSize();
		
		if (parentSize.width > mySize.width) 
			x = ((parentSize.width - mySize.width)/2) + topLeft.x;
		else 
			x = topLeft.x;
		
		if (parentSize.height > mySize.height) 
			y = ((parentSize.height - mySize.height)/2) + topLeft.y;
		else 
			y = topLeft.y;
		
		setLocation (x, y);
		super.setVisible(true);
		requestFocus();
	}  
}
