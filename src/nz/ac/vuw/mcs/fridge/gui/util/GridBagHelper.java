package nz.ac.vuw.mcs.fridge.gui.util;

import java.awt.GridBagConstraints;
import java.awt.Insets;

/**
 * This class returns GridBagConstraints, optionally with inset padding. It speeds up the use of
 * the GridBag layout manager!
 * @author neil
 *
 */
public class GridBagHelper {
	/**
	 * This method is STATIC and will give you a GridBagConstraints with the required attributes
	 * @param gridx Horizontal grid position of the element
	 * @param gridy Vertical grid position of the element
	 * @param gridwidth How many cells the element should span
	 * @param gridheight How many rows the element should span
	 * @param position either left, centre, right, top-left, top-right, bottom-left, bottom-right
	 * @return a GridBagConstraints object with the desired attributes.
	 */
	public static GridBagConstraints getConstraint(int gridx, int gridy, int gridwidth, int gridheight, String position) {
		GridBagConstraints tempGbc = new GridBagConstraints();
		tempGbc.gridx = gridx;
		tempGbc.gridy = gridy;
		tempGbc.gridwidth = gridwidth;
		tempGbc.gridheight = gridheight;
		
		if (position.equals("left")) {
			tempGbc.anchor = GridBagConstraints.LINE_START;
		}
		else if (position.equals("centre")) {
			tempGbc.anchor = GridBagConstraints.CENTER;
		}
		else if (position.equals("right")) {
			tempGbc.anchor = GridBagConstraints.LINE_END;
		}
		else if (position.equals("top-left")) {
			tempGbc.anchor = GridBagConstraints.FIRST_LINE_START;
		}
		else if (position.equals("top-right")) {
			tempGbc.anchor = GridBagConstraints.FIRST_LINE_END;
		}
		else if (position.equals("bottom-left")) {
			tempGbc.anchor = GridBagConstraints.LAST_LINE_START;
		}
		else if (position.equals("bottom-right")) {
			tempGbc.anchor = GridBagConstraints.LAST_LINE_END;
		}
		else {
			// error
			System.out.println("getConstraint was provided with an invalid position '" + position + "', returning null");
			return null;
		}
		
		return tempGbc;
	}
	
	/**
	 * This method is STATIC and will give you a GridBagConstraints with the required attributes
	 * @param gridx Horizontal grid position of the element
	 * @param gridy Vertical grid position of the element
	 * @param gridwidth How many cells the element should span
	 * @param gridheight How many rows the element should span
	 * @param position either left, centre, right, top-left, top-right, bottom-left, bottom-right
	 * @param insetTop How many pixels to pad the top of the element by
	 * @param insetLeft How many pixels to pad the left of the element by
	 * @param insetBottom How many pixels to pad the bottom of the element by
	 * @param insetRight How many pixels to pad the right of the element by
	 * @return a GridBagConstraints object with the desired attributes.
	 */
	public static GridBagConstraints getConstraintWithInsets(int gridx, int gridy, int gridwidth, int gridheight, String position, int insetTop, int insetLeft, int insetBottom, int insetRight) {
		GridBagConstraints tempGbc = getConstraint(gridx, gridy, gridwidth, gridheight, position);
		// add the insets
		tempGbc.insets = new Insets(insetTop, insetLeft, insetBottom, insetRight);
		return tempGbc;
	}
}
