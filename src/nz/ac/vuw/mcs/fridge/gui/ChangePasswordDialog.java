package nz.ac.vuw.mcs.fridge.gui;

import java.awt.AWTKeyStroke;
import java.awt.GridLayout;
import java.awt.KeyboardFocusManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.util.HashSet;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.KeyStroke;

import nz.ac.vuw.mcs.fridge.backend.DBWorker;
import nz.ac.vuw.mcs.fridge.gui.util.CentredModalJDialog;
/** 
 * This class extends JDialog and pops up a box to change the current logged-in user's password.
 * @author neil
 *
 */
public class ChangePasswordDialog extends CentredModalJDialog {
	/**
	 * Displays the dialog box and sets action handlers
	 */
	private static final long serialVersionUID = 1L;
	JDialog thisDialog = this;
	JPasswordField txtPass1 = new JPasswordField(10);
	JPasswordField txtPass2 = new JPasswordField(10);

	public ChangePasswordDialog() {		
		super(FridgeClient.mainWindow, "Change Password");
		
		this.getContentPane().setLayout(new GridLayout(3, 2));
		
		// labels
		JLabel label = new JLabel("New password");
		this.getContentPane().add(label);
		
		label = new JLabel("Confirm password");
		this.getContentPane().add(label);
		
		// password boxes
		txtPass1.setEchoChar('\u2022'); // bullet character
		
		HashSet<AWTKeyStroke> traversalKeys = new HashSet<AWTKeyStroke>();
		traversalKeys.add(AWTKeyStroke.getAWTKeyStroke(KeyEvent.VK_ENTER, 0, false));
		traversalKeys.add(AWTKeyStroke.getAWTKeyStroke(KeyEvent.VK_TAB, 0, false));
		txtPass1.setFocusTraversalKeys(KeyboardFocusManager.FORWARD_TRAVERSAL_KEYS, traversalKeys);
		
		this.getContentPane().add(txtPass1);
		
		txtPass2.setEchoChar('\u2022'); // bullet character
		txtPass2.registerKeyboardAction(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				doChange();
			}
		}, KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0), JComponent.WHEN_FOCUSED);
		this.getContentPane().add(txtPass2);
		
		// buttons
		JButton btnOK = new JButton("OK");
		
		this.getContentPane().add(btnOK);
		btnOK.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				doChange();
			}
		});
		
		final JButton btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// kill this dialog
				thisDialog.setVisible(false);
			}
		});
		this.getContentPane().add(btnCancel);
		
		this.pack();
		this.centreParent();
	}
	
	/**
	 * This method checks if the two passwords are equal, and if so, sends them to DBWorker.changeCurrentPassword
	 * to perform the actual change. It then presents the user with a confirmation box.
	 *
	 */
	private void doChange() {
		// check to make sure the passwords match
		String pass1 = new String(txtPass1.getPassword());
		String pass2 = new String(txtPass2.getPassword());
		
		if (pass1.equals(pass2)) {
			// they match. do the change
			DBWorker.changeCurrentUserPassword(pass1);
			thisDialog.setVisible(false);
			JOptionPane.showMessageDialog(thisDialog, "Your password has been changed", "Password changed", JOptionPane.INFORMATION_MESSAGE);
		}
		else {
			JOptionPane.showMessageDialog(thisDialog, "The passwords you entered did not match. Please try again.", "Passwords did not match", JOptionPane.ERROR_MESSAGE);
			txtPass1.requestFocus();
			txtPass1.selectAll();
		}
	}
}

