package nz.ac.vuw.mcs.fridge.gui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;
import javax.swing.table.AbstractTableModel;

import nz.ac.vuw.mcs.fridge.backend.DBWorker;
import nz.ac.vuw.mcs.fridge.backend.model.FridgeTransaction;
import nz.ac.vuw.mcs.fridge.backend.util.MoneyHelper;

public class TransactionFeedPanel extends JPanel {
	private static final long serialVersionUID = 1L;
	
	public TransactionFeedPanel() {
		// create titled border
		TitledBorder borderWithTitle = BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED), "Transaction Feed");
		this.setBorder(borderWithTitle);
	
		table = new JTable(tableModel);
		
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION); // allow single row selection
		table.setPreferredScrollableViewportSize(new Dimension(FridgeClient.mainWindow.getWidth() - 300, FridgeClient.mainWindow.topPanel.getHeight() - 170));
		
		// set the col widths
		table.getColumnModel().getColumn(0).setPreferredWidth(300); // code
		table.getColumnModel().getColumn(1).setPreferredWidth(60); // descr
		table.getColumnModel().getColumn(2).setPreferredWidth(120); // price
		
		this.setLayout(new BorderLayout());
		this.add(new JScrollPane(table), BorderLayout.CENTER);
		
		populate();
	}
	
	public void refresh() {
		populate();
	}
	
	private void populate() {
		transactions = DBWorker.getTransactionFeed();
		tableModel.fireTableDataChanged();
	}
	
	private JTable table;
	private List<FridgeTransaction> transactions;
	private String [] colNames = { "Date", "Amount", "Type" };
	
	AbstractTableModel tableModel = new AbstractTableModel() {
		private static final long serialVersionUID = 1L;

		public String getColumnName(int col) {
			return colNames[col];
		}

		public int getRowCount() {
			return (transactions!=null)?transactions.size():0;
		}

		public int getColumnCount() {
			return colNames.length;
		}

		public Object getValueAt(int row, int col) {
			FridgeTransaction transaction = transactions.get(row);
			switch (col) {
				case (0): return transaction.date;
				case (1): return MoneyHelper.formatCurrency(transaction.amount);
				case (2): return transaction.type;
				default: return null;
			}
		}

		public boolean isCellEditable(int row, int col) {
			return false;
		}
	};
}
