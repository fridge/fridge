package nz.ac.vuw.mcs.fridge.gui;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.GridBagLayout;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

import nz.ac.vuw.mcs.fridge.backend.DBWorker;
import nz.ac.vuw.mcs.fridge.backend.model.ProductCategory;
import nz.ac.vuw.mcs.fridge.backend.model.StockItem;
import nz.ac.vuw.mcs.fridge.backend.util.MoneyHelper;
import nz.ac.vuw.mcs.fridge.gui.util.GridBagHelper;

public class ProductsPanel extends JPanel implements MouseListener {
	private static final long serialVersionUID = 1L;

	public ProductsPanel() {
		// set our layout
		this.setLayout(new GridBagLayout());
		
		// set our titled border
		TitledBorder borderWithTitle = BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED), "Products");
		this.setBorder(borderWithTitle);
		
		this.populatePanelFromDB();
	}

	public void populatePanelFromDB() {
		this.setVisible(false);

		// Clear the panel of any previous stuff
		this.removeAll();

		// Get a list of categories
		List<ProductCategory> categories = DBWorker.getCategories();

		// Now we iterate through the categories list, listing products of each category
		int column = 0;
		int row = 0;

		for (int i=0; i < categories.size(); i++) {
			// reset the row, set column equal to i * 3 (3 cols per category)
			row = 0;
			column = i * 3;
			// make a JLabel for the category name
			ProductCategory thisCategory = categories.get(i);
			JLabel categoryHeader = new JLabel("<html><u><b>" + thisCategory.getTitle() + "</b></u></html>");
			this.add(categoryHeader, GridBagHelper.getConstraint(column, row, 3, 1, "centre"));

			// get products matching this category.
			List<StockItem> categoryProducts = DBWorker.getProductsForCategory(thisCategory);
			// get the categories out of the db in the order of the sequence_number of the row
			for (StockItem product: categoryProducts) {
				// increment the row
				row++;

				String thisCode = product.productCode;

				// make 3 JLabel's for this item, position and add them
				JLabel lblCode = new JLabel("<html><b>" + thisCode + "</b></html>");
				JLabel lblDescr = new JLabel(product.description);
				JLabel lblPrice = new JLabel(MoneyHelper.formatCurrency(product.price / 100.0));

				// set names and event listeners
				lblCode.setName(thisCode);
				lblDescr.setName(thisCode);
				lblPrice.setName(thisCode);
				lblCode.addMouseListener(this);
				lblDescr.addMouseListener(this);
				lblPrice.addMouseListener(this);

				// set colours, grey=0 stock, red=negative stock, ignore otherwise
				if (product.inStock == 0) {
					// set to grey
					Color grey = new Color(102, 102, 133);
					lblCode.setForeground(grey);
					lblDescr.setForeground(grey);
					lblPrice.setForeground(grey);
				}
				else if (product.inStock < 0) {
					// set to red
					Color red = new Color(233, 0, 0);
					lblCode.setForeground(red);
					lblDescr.setForeground(red);
					lblPrice.setForeground(red);
				}

				this.add(lblCode, GridBagHelper.getConstraintWithInsets(column, row, 1, 1, "right", 0, 20, 0, 5));
				this.add(lblDescr, GridBagHelper.getConstraintWithInsets(column + 1, row, 1, 1, "left", 0, 5, 0, 5));
				this.add(lblPrice, GridBagHelper.getConstraintWithInsets(column + 2, row, 1, 1, "left", 0, 5, 0, 20));
			}
			this.setVisible(true);
		}
	}

	// mouse event handlers
	public void mouseClicked(MouseEvent itemClickedByMouseEvent) {
		// if there is a current order, add to it (clicks do nothing when login panel is up)
		if (FridgeClient.mainWindow.currentOrder != null) {
			FridgeClient.mainWindow.currentOrder.addItem(itemClickedByMouseEvent.getComponent().getName(), 1);
		}
	}
	public void mousePressed(MouseEvent e) {
		// ignore
	}
	public void mouseReleased(MouseEvent e) {
		// ignore
	}
	public void mouseEntered(MouseEvent e) {
		// set cursor to a hand, if there is a current order
		if (FridgeClient.mainWindow.currentOrder != null) {
			setCursor(new Cursor(Cursor.HAND_CURSOR));
		}
	}
	public void mouseExited(MouseEvent e) {
		// reset cursor to normal (whether or not it has changed)
		setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
	}
}
