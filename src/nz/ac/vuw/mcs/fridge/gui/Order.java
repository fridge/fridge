/*
 * Created on Mar 26, 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package nz.ac.vuw.mcs.fridge.gui;

import java.awt.AWTKeyStroke;
import java.awt.Dimension;
import java.awt.KeyboardFocusManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.util.HashSet;
import java.util.Vector;

import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.KeyStroke;
import javax.swing.ListSelectionModel;
import javax.swing.table.AbstractTableModel;

import nz.ac.vuw.mcs.fridge.backend.DBWorker;
import nz.ac.vuw.mcs.fridge.backend.InterfridgeException;
import nz.ac.vuw.mcs.fridge.backend.model.OrderLine;
import nz.ac.vuw.mcs.fridge.backend.util.MoneyHelper;

/**
 * @author neil
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class Order {
	public Order() {
		orderLines = new Vector<OrderLine>();
		colNames = new String[]{"Code", "Description", "Unit Cost", "Quantity", "Total Cost"};
		table = new JTable(tableModel);
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION); // allow single row selection
		table.setPreferredScrollableViewportSize(new Dimension(FridgeClient.mainWindow.getWidth() - 300, FridgeClient.mainWindow.topPanel.getHeight() - 170));
		
		// set the col widths
		table.getColumnModel().getColumn(0).setPreferredWidth(60); // code
		table.getColumnModel().getColumn(1).setPreferredWidth(300); // descr
		table.getColumnModel().getColumn(2).setPreferredWidth(60); // price
		table.getColumnModel().getColumn(3).setPreferredWidth(60); // qty
		table.getColumnModel().getColumn(0).setPreferredWidth(60); // totalcost
		
		// Set relevant keys
		table.registerKeyboardAction(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// the user pressed DELETE
				// we need to delete the selected row
				// we fetch the product code from *selectedrow*, 0.
				deleteProduct((String)table.getValueAt(table.getSelectedRow(), 0));
				tableModel.fireTableDataChanged();
				FridgeClient.mainWindow.addToOrderPanel.setFocusOnProductCode();
			}
		}, KeyStroke.getKeyStroke(KeyEvent.VK_DELETE, 0), JComponent.WHEN_IN_FOCUSED_WINDOW);
		
		// set the focus change keys
		HashSet<AWTKeyStroke> traversalKeys = new HashSet<AWTKeyStroke>();
		traversalKeys.add(AWTKeyStroke.getAWTKeyStroke(KeyEvent.VK_TAB, 0, false));
		traversalKeys.add(AWTKeyStroke.getAWTKeyStroke(KeyEvent.VK_UP, 0, false)); // up arrow key
		table.setFocusTraversalKeys(KeyboardFocusManager.FORWARD_TRAVERSAL_KEYS, traversalKeys);
		
		traversalKeys = new HashSet<AWTKeyStroke>();
		traversalKeys.add(AWTKeyStroke.getAWTKeyStroke(KeyEvent.VK_TAB, InputEvent.SHIFT_DOWN_MASK, false)); // shift-tab
		table.setFocusTraversalKeys(KeyboardFocusManager.BACKWARD_TRAVERSAL_KEYS, traversalKeys);
		
		table.addFocusListener(new FocusListener() {
			public void focusGained(FocusEvent e) {
				// select the first row when we get focus from a keyboard focus event, NOT a mouse one!!
				// (mouse will select a row directly, so selection will be >=0)
				if (table.getSelectedRow() < 0) {
					table.changeSelection(0, 0, false, false);
				}
			}
			public void focusLost(FocusEvent e) {
			// deselect all rows
				table.clearSelection();
			}
		});
		
		// TAB/SHIFT-TAB goes back to code field
		table.registerKeyboardAction(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				// TODO this should finalise the order according to chris
				FridgeClient.mainWindow.addToOrderPanel.setFocusOnProductCode();
			}
		}, KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0), JComponent.WHEN_IN_FOCUSED_WINDOW);
	}
	public void addItem(String productCode, int qty) {
		// if qty is 0, ignore this request
		if (qty == 0) {
			return;
		}
		
		// Enable the submit button in addItemToOrderPanel
		FridgeClient.mainWindow.addToOrderPanel.enableSubmitButton();
		
		// check to see if we already have this item, if so, increment it
		incrementOrderCost(DBWorker.getPriceForCode(productCode) * qty);
		for (int i = 0; i < orderLines.size(); i++) {
			OrderLine tempLine = (OrderLine)orderLines.get(i);
			if (tempLine.getCode().equals(productCode)) {
				tempLine.incrementQty(qty);
				orderLines.set(i, tempLine);
				updateTable();
				return;
			}
		}
		// if we get here, we don't have this row. Insert it.
		OrderLine newLine = new OrderLine(productCode, qty);
		orderLines.add(newLine);
		updateTable();
	}
	public JTable getTable() {
		return table;
	}
	public void deleteProduct(String productCodeToDelete) {
		for (int i = 0; i < orderLines.size(); i++) {
			OrderLine tempLine = (OrderLine)orderLines.get(i);
			if (tempLine.getCode().equals(productCodeToDelete)) {
				incrementOrderCost(-1 * (DBWorker.getPriceForCode(tempLine.getCode()) * tempLine.getQty()));
				orderLines.removeElementAt(i);
				updateTable();
				return;
			}
		}
		// eek! it wasn't there
		System.out.println("ERROR! An orderLines row was supposed to delete, but the line wasn't there!");
	}
	public void updateTable() {
		tableModel.fireTableDataChanged();
	}
	public void incrementOrderCost(double by) {
		totalOrderCost += by;
		// update the form field
		FridgeClient.mainWindow.currentOrderPanel.userStats.updateOrderAmount(totalOrderCost);
	}
	public void submitOrder(boolean confirmFirst) {
		// make sure this transfer is within their credit limit
		if (!DBWorker.checkIfTransactionIsWithinCreditLimit(totalOrderCost)) {
			// they don't have enough balance, find what balance they have
			double currentBal = DBWorker.getCurrentUserBalance();
			double requiredTopUp = totalOrderCost - currentBal;
			
			JOptionPane.showMessageDialog(FridgeClient.mainWindow, "Sorry, but you don't have enough money to make that purchase\nYou have " + MoneyHelper.formatCurrency(currentBal) + " so you need to top up by " + MoneyHelper.formatCurrency(requiredTopUp) + " to make this purchase.", "Insufficient funds", JOptionPane.ERROR_MESSAGE);
			return;
		}
		
		if (orderLines.size() > 0) {
			if (confirmFirst) {
				String oldBalance = MoneyHelper.formatCurrency(DBWorker.getCurrentUserBalance());
				String orderAmount = MoneyHelper.formatCurrency(totalOrderCost);
				double afterbal = DBWorker.getCurrentUserBalance() - totalOrderCost;
				String balanceAfter = MoneyHelper.formatCurrency(afterbal);

				String message = "You are about to submit an order.\nYour old balance: " + oldBalance + "\nOrder amount: " + orderAmount + "\nNew balance: " + balanceAfter + "\nProceed?";			
				if (JOptionPane.showConfirmDialog(FridgeClient.mainWindow, message, "Confirmation", JOptionPane.OK_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE) != JOptionPane.OK_OPTION){
					return; // send them back to their order if they cancel the confirmation
				}
			}
			//Go ahead and submit, confirmation has been made if required 
			try {
				DBWorker.submitOrder(this);
			}
			catch (InterfridgeException e) {
				JOptionPane.showMessageDialog(FridgeClient.mainWindow, e.getMessage(), "Error submitting order", JOptionPane.ERROR_MESSAGE);
				return;
			}
		}
		
		// reset the form (clears current user ID etc too)
		FridgeClient.mainWindow.resetToLogin();
	}
	
	// member vars
	AbstractTableModel tableModel = new AbstractTableModel() {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		public String getColumnName(int col) {
			return colNames[col];
		}
		public int getRowCount() {
			return orderLines.size();
		}
		public int getColumnCount() {
			return colNames.length;
		}
		public Object getValueAt(int row, int col) {
			OrderLine tempLine = (OrderLine)orderLines.get(row);
			String currentCode = tempLine.getCode();
			switch (col) {
				case (0): return currentCode;
				case (1): return DBWorker.getDescriptionForCode(currentCode);
				case (2): return MoneyHelper.formatCurrency(DBWorker.getPriceForCode(currentCode));
				case (3): return String.valueOf(tempLine.getQty());
				case (4): return MoneyHelper.formatCurrency(tempLine.getQty() * DBWorker.getPriceForCode(currentCode));
				default: return null;
			}
		}
		public boolean isCellEditable(int row, int col) {
			return false;
		}
	};

	private JTable table;	
	private Vector<OrderLine> orderLines;
	private String[] colNames;
	private double totalOrderCost = 0;

	public double getTotalOrderCost() {
		return totalOrderCost;
	}

	public Vector<OrderLine> getOrderLines() {
		return orderLines;
	}
}
