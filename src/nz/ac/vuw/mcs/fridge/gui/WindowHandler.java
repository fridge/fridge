package nz.ac.vuw.mcs.fridge.gui;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.BoxLayout;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.KeyStroke;

import nz.ac.vuw.mcs.fridge.Config;
import nz.ac.vuw.mcs.fridge.backend.DBWorker;
import nz.ac.vuw.mcs.fridge.backend.InterfridgeException;

public class WindowHandler extends JFrame {
	private static final long serialVersionUID = 1L;
	
	// components of the main window
	final LoginPanel loginPanel = new LoginPanel();
	final WorstFridgeOffenders badPeople = new WorstFridgeOffenders();
	final AddItemToOrderPanel addToOrderPanel = new AddItemToOrderPanel();
	final ProductsPanel productListPanel = new ProductsPanel();
	final AdminListPanel adminList = new AdminListPanel();
	final MenuBar menuBar = new MenuBar();
	OrderPanel currentOrderPanel;
	TransactionFeedPanel transactionFeedPanel;
	Order currentOrder;
	
	// holds the top half of the window (everything except productListPanel)
	final JPanel topPanel = new JPanel();
	
	// constructor
	public WindowHandler() {
		// configures the common window features
		
		// turn on default swing look and feel
		JFrame.setDefaultLookAndFeelDecorated(true);
		
		// set the layout manager of the main window to a vertical boxlayout
		this.getContentPane().setLayout(new BoxLayout(this.getContentPane(), BoxLayout.PAGE_AXIS));
		
		// initialise the top panel
		topPanel.setLayout(new BoxLayout(topPanel, BoxLayout.PAGE_AXIS));
		topPanel.setPreferredSize(new Dimension(800, 300)); // width, height of top part of window
		
		// if the unit presses ESCAPE, the program will reset to the login window. (cancel order)
		topPanel.registerKeyboardAction(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				resetToLogin();
			}
		}, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_IN_FOCUSED_WINDOW);
		
		if (Config.ONLY_ADMIN_MAY_CLOSE) {
			// set close 'X' button behaviour to disable non-admins from closing the window
			this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
			this.addWindowListener(new WindowListener() {
				public void windowActivated(WindowEvent e) {
				}

				public void windowClosed(WindowEvent e) {
				}

				public void windowClosing(WindowEvent e) {
					// if there is an order up, cancel it and return to the login window
					if (currentOrder != null) {
						resetToLogin();
					}
					else {
						// they're trying to close the login screen. tell the admin user to login then select quit from menu
						JOptionPane.showMessageDialog(FridgeClient.mainWindow, "To quit FridgeClient, login as an admin user then select quit from the options menu.", "Only admin can do that", JOptionPane.INFORMATION_MESSAGE);
					}
				}

				public void windowDeactivated(WindowEvent e) {
				}

				public void windowDeiconified(WindowEvent e) {
				}

				public void windowIconified(WindowEvent e) {
				}

				public void windowOpened(WindowEvent e) {
				}
			});
		}
	}
	
	public void displayWindowWithLoginPanel() {
		// displays the main window frame with the login panel and product list panel
		
		topPanel.add(loginPanel);
		topPanel.add(badPeople);
		topPanel.add(adminList);
		
		this.getContentPane().add(topPanel);
		//this.getContentPane().add(loginPanel);
		this.getContentPane().add(productListPanel);
		this.setTitle("Welcome to fridge - Please login");

		// update number served on login screen!
		loginPanel.updateServed();
		
		this.setResizable(true);
		this.pack();
		if (Config.START_MAXIMISED) {
			this.setExtendedState(JFrame.MAXIMIZED_BOTH);
		}
		this.setVisible(true);
	}
	public void switchToOrderMode() {
		// Time the window transition if desired
		//TimeCounter timer = new TimeCounter();
		//timer.startTimer();
		
		// remove the login panel (all panels) from the top part of the window
		topPanel.setVisible(false);
		topPanel.removeAll();
		
		// instanciate a new Order object
		currentOrder = new Order();
		
		// add the order panel
		topPanel.add(addToOrderPanel);
		
		// add the order panel
		currentOrderPanel = new OrderPanel();
		transactionFeedPanel = new TransactionFeedPanel();
		
		UserPanel userPanel = new UserPanel();
		topPanel.add(userPanel);
		
		// add the menu bar
		this.setJMenuBar(menuBar);
		
		// re-enable the top half of the window
		topPanel.setVisible(true);
		
		// set the product code entry box in focus
		addToOrderPanel.setFocusOnProductCode();
		
		// update the products list on login
		productListPanel.populatePanelFromDB();
		
		// change the window title
		String realName = "(error getting name)";
		try {
			realName = DBWorker.getCurrentUserRealName();
		}
		catch (InterfridgeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.setTitle("Fridge - Order for " + realName);
		//System.out.println("Timer: " + timer.stopTimer());
				
		// Reset the top panel state (clears out textboxes and sets submit button disabled)
		// Added by Neil 5/8/05
		addToOrderPanel.clearFields();
	}
	
	public void resetToLogin() {
		// swaps the top panel back to the login panel and changes the window title back
		DBWorker.forgetUser();
		addToOrderPanel.clearFields();
		currentOrder = null;
		
		// kill the menubar
		this.setJMenuBar(null);
		
		topPanel.setVisible(false);
		topPanel.removeAll();
		
		// update number served on login screen!
		loginPanel.updateServed();
		
		topPanel.add(loginPanel);
		topPanel.add(badPeople);
		topPanel.add(adminList);
		
		// update the products panel on logout unconditionally
		// this is so that when stock drops to < 1 it shows up on the products panel
		productListPanel.populatePanelFromDB();
		badPeople.refresh();
		
		topPanel.setVisible(true);
		FridgeClient.mainWindow.setTitle("Welcome to fridge - Please login");
		FridgeClient.mainWindow.requestFocus(); // focus window
		loginPanel.txtUsername.requestFocus(); // focus username
	}
}
