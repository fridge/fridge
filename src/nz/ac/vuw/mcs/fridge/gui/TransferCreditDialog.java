package nz.ac.vuw.mcs.fridge.gui;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowFocusListener;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.KeyStroke;

import nz.ac.vuw.mcs.fridge.backend.DBWorker;
import nz.ac.vuw.mcs.fridge.backend.InterfridgeException;
import nz.ac.vuw.mcs.fridge.backend.util.MoneyHelper;
import nz.ac.vuw.mcs.fridge.gui.util.CentredModalJDialog;

public class TransferCreditDialog extends CentredModalJDialog {
	/**
	 * Display a dialog box that allows the currently logged in user
	 * to transfer credit to another valid user.
	 */
	final JDialog thisWindow = this;
	private static final long serialVersionUID = 1L;

	public TransferCreditDialog() {		
		// create ourselves as a modal dialog
		super(FridgeClient.mainWindow, "Transfer Credit");
		
		final JPanel creditTransferPanel = new JPanel(new GridLayout(3, 2));
		JLabel label = new JLabel("Transfer Amount");
		creditTransferPanel.add(label);
		label = new JLabel("Recipient user code");
		creditTransferPanel.add(label);
		final JTextField txtAmount = new JTextField(10);
		final JTextField txtRecipient = new JTextField(10);
		final JButton btnOK = new JButton("OK");
		// transfer focus on pressing enter
		txtAmount.registerKeyboardAction(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				txtRecipient.requestFocus();
			}
		}, KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0), JComponent.WHEN_FOCUSED);
		creditTransferPanel.add(txtAmount);
		txtRecipient.registerKeyboardAction(new ActionListener() {	
		public void actionPerformed(ActionEvent e) {
				btnOK.doClick();
			}
		}, KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0), JComponent.WHEN_FOCUSED);
		creditTransferPanel.add(txtRecipient);
		btnOK.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// check input and prompt with dialog, then do it
				double amount = 0;
				try {
					amount = Double.parseDouble(txtAmount.getText());
				}
				catch (NumberFormatException numExc) {
					JOptionPane.showMessageDialog(thisWindow, "That amount is not a valid number.", "Invalid Amount", JOptionPane.ERROR_MESSAGE);
					return;
				}
				// make sure they don't try to transfer a negative amount to someone!
				if (amount < 0) {
					JOptionPane.showMessageDialog(thisWindow, "You can't transfer a negative amount!.", "Invalid Amount", JOptionPane.ERROR_MESSAGE);
					return;
				}
				
				// make sure this transfer is within their credit limit
				if (!DBWorker.checkIfTransactionIsWithinCreditLimit(amount)) {
					// they don't have enough balance
					double currentBal = DBWorker.getCurrentUserBalance();
					double requiredTopUp = amount - currentBal;
					JOptionPane.showMessageDialog(thisWindow, "Sorry, but you don't have enough money to make that transfer\nYour balance is " + MoneyHelper.formatCurrency(currentBal) + " so you need to top up by " + MoneyHelper.formatCurrency(requiredTopUp) + " to make this transfer.", "Insufficient funds", JOptionPane.ERROR_MESSAGE);
					return;
				}
				
				if (DBWorker.validateUserCode(txtRecipient.getText())) {
					// valid recipient. make sure they're sure
					String question = "Are you sure you want to transfer $" + amount + " to usercode " + txtRecipient.getText() + "?";
					if (JOptionPane.showConfirmDialog(thisWindow, question, "Tranfer Confirmation", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE) == JOptionPane.YES_OPTION) {
						// they said yes
						thisWindow.setVisible(false);
						try {
							DBWorker.transferFunds(amount, txtRecipient.getText());

							// update the label that shows the user what their current balance is
							FridgeClient.mainWindow.currentOrderPanel.userStats.updateUserBalance();

							// show confirmation dialog
							JOptionPane.showMessageDialog(FridgeClient.mainWindow, "Funds transferred successfully", "Transfer complete", JOptionPane.INFORMATION_MESSAGE);
						}
						catch (InterfridgeException ex) {
							//Show error
							JOptionPane.showMessageDialog(FridgeClient.mainWindow, ex.getMessage(), "Error transferring", JOptionPane.ERROR_MESSAGE);
						}
					}
					else {
						thisWindow.setVisible(false);
					}
				}
				else {
					// usercode is invalid
					JOptionPane.showMessageDialog(thisWindow, "That usercode is not valid", "Invalid Usercode", JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		creditTransferPanel.add(btnOK);
		final JButton btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// close the dialog
				thisWindow.setVisible(false);
			}
		});
		creditTransferPanel.add(btnCancel);
		thisWindow.addWindowFocusListener(new WindowFocusListener() {
			public void windowGainedFocus(WindowEvent e) {
				// when we gain focus, focus the first field and enable all fields
				txtAmount.requestFocus();
			}
			public void windowLostFocus(WindowEvent e) {
			}
		}); 
		creditTransferPanel.registerKeyboardAction(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnCancel.doClick();
			}
		}, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_IN_FOCUSED_WINDOW);
		this.getContentPane().add(creditTransferPanel);
		this.pack();
		this.centreParent();
	}
}
