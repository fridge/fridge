package nz.ac.vuw.mcs.fridge.gui;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

import nz.ac.vuw.mcs.fridge.backend.DBWorker;
import nz.ac.vuw.mcs.fridge.backend.InterfridgeException;

/**
 * Shows a list of administrators on the login screen
 */
public class AdminListPanel extends JPanel {
	private static final long serialVersionUID = 1L;

	public AdminListPanel() {
		TitledBorder borderWithTitle = BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED), "Getting an account");
		this.setBorder(borderWithTitle);

		String messageString = "<html>Error getting signup information.</html>";

		try {
			messageString = "<html>" + DBWorker.getSignupInfo().replace("\n", "<br>") + "</html>";
		}
		catch (InterfridgeException e) {
			System.out.println("Non-fatal error: Couldn't get signup information. Exception:\n" + e);
		}

		JLabel message = new JLabel(messageString);
		this.add(message);
	}
}
