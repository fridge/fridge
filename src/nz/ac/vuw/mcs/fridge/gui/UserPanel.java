package nz.ac.vuw.mcs.fridge.gui;

import java.awt.BorderLayout;
import java.awt.GridLayout;

import javax.swing.JPanel;

public class UserPanel extends JPanel {
	private static final long serialVersionUID = 1L;

	public UserPanel() {
		//TitledBorder borderWithTitle = BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED), "Order");
		//this.setBorder(borderWithTitle);

		// use boxlayout
		this.setLayout(new BorderLayout());

		JPanel left = new JPanel();
		left.setLayout(new GridLayout(2, 1));

		this.add(left, BorderLayout.CENTER);

		// create the scrollpane to show the table
		left.add(FridgeClient.mainWindow.currentOrderPanel);

		//create the transaction feed
		left.add(FridgeClient.mainWindow.transactionFeedPanel);

		// add the info panel
		this.add(FridgeClient.mainWindow.currentOrderPanel.userStats, BorderLayout.EAST);
	}
}
