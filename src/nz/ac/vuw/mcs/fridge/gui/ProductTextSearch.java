package nz.ac.vuw.mcs.fridge.gui;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.util.List;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.BoxLayout;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.KeyStroke;
import javax.swing.ListSelectionModel;

import nz.ac.vuw.mcs.fridge.backend.DBWorker;
import nz.ac.vuw.mcs.fridge.backend.model.StockItem;

public class ProductTextSearch {
	/**
	 * This constructor will perform a text search of product descriptions, displaying a dialog from which the user can
	 * pick, or press escape to cancel
	 * 
	 * @param searchString is the string to hunt for
	 * @return the product code if selected, or null if nothing found or user presses cancel
	 */
	public ProductTextSearch(String searchString) {
		// we'll prepare the SQL ourselves, so strip any character other than alphanumeric and dot
		// replace them with SQL wildcard characters (multiple illegal chars convert to single %)
		
		//if (searchString.equals("INVALID")) return; // if prog loses focus, it'll search for INVALID maybe
		
		Pattern pattern = Pattern.compile("[^a-zA-Z0-9\\.\\s]+");
		Matcher matcher = pattern.matcher(searchString);
		StringBuffer buf = new StringBuffer();
		boolean found = false;
	    while ((found = matcher.find())) {
	        // Insert replacement
	        matcher.appendReplacement(buf, "%");
	    }
	    matcher.appendTail(buf);
	    
	    // Get result
	    searchString = buf.toString();

	    List<StockItem> matches = DBWorker.getProductsMatchingSearch(searchString);

	    Vector<String> results = new Vector<String>();
	    for (StockItem item: matches) {
	    	results.add("[" + item.productCode + "]\t" + item.description);
	    }

	    if (results.size() == 0) {
	    	FridgeClient.mainWindow.addToOrderPanel.setInvalid(); // no matches
	    	return;
	    }

	    // spawn a dialog box
	    final JDialog searchResults = new JDialog(FridgeClient.mainWindow, "Search Results", true); // modal dialog
	    JPanel content = new JPanel();
	    content.setLayout(new BoxLayout(content, BoxLayout.PAGE_AXIS));
	    content.add(new JLabel("Select a match below, or press ESCAPE to cancel"));

	    final JList selectionList = new JList(results);

	    // set event handlers for enter and escape keys
	    selectionList.registerKeyboardAction(new ActionListener() {
	    	public void actionPerformed(ActionEvent e) {
	    		String selectedRow = (String)selectionList.getSelectedValue();
	    		// extract the product code from within the [CODE]
	    		int closeBracket = selectedRow.indexOf(']');
	    		String code = selectedRow.substring(1, closeBracket);

	    		// Process this code
	    		FridgeClient.mainWindow.addToOrderPanel.setProductCodeText(code);
	    		FridgeClient.mainWindow.addToOrderPanel.setFocusOnProductCode();

	    		searchResults.setVisible(false);
	    	}
	    }, KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0), JComponent.WHEN_IN_FOCUSED_WINDOW);

	    selectionList.registerKeyboardAction(new ActionListener() {
	    	public void actionPerformed(ActionEvent e) {
	    		searchResults.setVisible(false);
	    		FridgeClient.mainWindow.addToOrderPanel.setInvalid();
	    	}
	    }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_IN_FOCUSED_WINDOW);

	    selectionList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
	    JScrollPane listScroller = new JScrollPane(selectionList);
	    listScroller.setPreferredSize(new Dimension(300, 200));	

	    content.add(listScroller);

	    // set first row selected in list
	    selectionList.setSelectedIndex(0);

	    searchResults.getContentPane().add(content);
	    searchResults.pack();
	    searchResults.setVisible(true);
	}
}
