package nz.ac.vuw.mcs.fridge.gui;

import java.awt.BorderLayout;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

public class OrderPanel extends JPanel {
	private static final long serialVersionUID = 1L;

	final UserStatsPanel userStats = new UserStatsPanel();
	
	public OrderPanel() {
		// this panel is composite, containing the order table on the left, and the user balance on the right
		// create titled border
		TitledBorder borderWithTitle = BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED), "Order");
		this.setBorder(borderWithTitle);
	
		// use boxlayout
		this.setLayout(new BorderLayout());
		
		// create the scrollpane to show the table
		this.add(new JScrollPane(FridgeClient.mainWindow.currentOrder.getTable()), BorderLayout.CENTER); //GridBagHelper.getConstraintWithInsets(0, 0, 0, 0, "top-left", 10, 10, 10, 50));

	}
}
