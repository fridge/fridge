package nz.ac.vuw.mcs.fridge;

public class Config {
	//UI options
	public static final boolean ONLY_ADMIN_MAY_CLOSE = false;
	public static final boolean START_MAXIMISED = true;

	//Fridge server options
	/**
	 * The name of this local fridge, used to login to remote fridges for interfridge.
	 */
	public static final String INTERFRIDGE_NAME = "memphis";
	public static final String FRIDGE_SERVER = "http://memphisfridge.interface.org.nz/fridgeserver.php";
}
