/*
 * Created on Mar 26, 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package nz.ac.vuw.mcs.fridge.backend;

import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import nz.ac.vuw.mcs.fridge.Config;
import nz.ac.vuw.mcs.fridge.backend.model.AuthenticatedUser;
import nz.ac.vuw.mcs.fridge.backend.model.FridgeTransaction;
import nz.ac.vuw.mcs.fridge.backend.model.ProductCategory;
import nz.ac.vuw.mcs.fridge.backend.model.StockItem;
import nz.ac.vuw.mcs.fridge.backend.model.User;
import nz.ac.vuw.mcs.fridge.gui.Order;

/**
 * @author neil
 *
 * This class does all the work that involves the database (except drawing the product list labels in
 * ProductsPanel, which accesses the DB itself. All statements are PreparedStatements, predeclared in the constructor
 * and filled in by the functions below. This ensures no illegal characters are allowed into the sql string.
 * <p>
 * The database configuration is specified as member variables up the top of the declaration.
 * 
 */
public class DBWorker {
	private static Fridge localFridge;
	private static AuthenticatedUser currentUser;
	private static String message;

	/**
	 * Cached list of stocked items. This will be updated when a user logs in or out, to reflect their particular list or the general list respectively.
	 */
	private static Map<String, StockItem> cachedStock;

	static {
		try {
			localFridge = new Fridge(Config.INTERFRIDGE_NAME, Config.FRIDGE_SERVER);
			cachedStock = localFridge.getStock();
		}
		catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.exit(1);
		}
		catch (InterfridgeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.exit(1);
		}
	}

	public static String message() {
		if (message == null) return "";
		String msg = message;
		message = null;
		return msg;
	}

	/**
	 * Authenticates a user, be they local or interfridge.
	 * If the user is successfully authenticated, sets the current user to that user.
	 * 
	 * @param user is the username of the user
	 * @param pass is the password of the user
	 * @return true if the combination is correct
	 */
	public static boolean authoriseUser(String user, String pass) {
		currentUser = null;
		try {
			currentUser = localFridge.parseAuthenticateUser(user, pass);
			//Update stock list to take account of the logged-in user
			cachedStock = localFridge.getStockForUser(currentUser);
		}
		catch (InterfridgeException e) {
			//Display the error on the login screen
			message = e.getMessage();
		}
		catch (IllegalArgumentException e) {
			message = e.getMessage();
		}
		return currentUser != null;
	}

	/**
	 * Gets the account balance in dollars of the currently logged-in user
	 * @return Account balance of currently logged-in user
	 */
	public static double getCurrentUserBalance() {
		return currentUser.getBalance() / 100.0;
	}

	/***
	 * Gets a description from the database for a given product code
	 * @param productCode the code to lookup
	 * @return the description of the provided code
	 * @throws InterfridgeException 
	 */
	public static String getDescriptionForCode(String productCode) {
		return cachedStock.get(productCode).description;
	}
	
	/**
	 * Get the price for a given product code
	 * @param productCode is the code to find the price for
	 * @return the price of the given code
	 * @throws InterfridgeException 
	 */
	public static double getPriceForCode(String productCode) {
		return cachedStock.get(productCode).price / 100.0;
	}
	
	/**
	 * Gets the real name for the currently logged-in user. Not used at present.
	 * @return real name of the currently user
	 * @throws InterfridgeException 
	 */
	public static String getCurrentUserRealName() throws InterfridgeException {
		return currentUser.getRealName();
	}

	/**
	 * Gets the last 15 transactions for the current user.
	 * @return List of transactions
	 */
	public static List<FridgeTransaction> getTransactionFeed() {
		return currentUser.getTransactionFeed();
	}
	
	/**
	 * Checks if a given product code or barcode exists.
	 * @param productCode is the code to check
	 * @return the code it found (in case it was a barcode) if it exists, otherwise null.
	 */
	public static String validateProductCode(String productCode) {
		if (cachedStock.containsKey(productCode)) {
			return productCode;
		}
		else {
			//TODO: Add support for barcodes too
			return null;
		}
	}

	/**
	 * Checks if the currently logged-in user has administrator privs.
	 * Used currently to see if they're allowed to quit.
	 * @return true if the logged-in user is an administrator
	 */
	public static boolean isCurrentUserAdmin() throws InterfridgeException {
		return currentUser.isAdmin();
	}

	public static void transferFunds(double amount, String recipientUsername) throws InterfridgeException {
		User user;
		int at = recipientUsername.indexOf('@');
		if (at == -1) {
			user = localFridge.getUser(recipientUsername);
		}
		else {
			Fridge fridge = localFridge.getRemoteFridge(recipientUsername.substring(at + 1));
			if (fridge == null) {
				//No such fridge
				//TODO: How to handle this nicely?
				throw new IllegalArgumentException("No such fridge '" + recipientUsername.substring(at + 1) + "'");
			}
			user = fridge.getUser(recipientUsername.substring(0, at));
		}
		currentUser.transfer(user, (int) (amount * 100));
	}

	public static void forgetUser() {
		// for security, forget the current user
		currentUser = null;
		try {
			cachedStock = localFridge.getStock();
			//While we are at it, refresh cached messages for login screen to show
			localFridge.refreshMessages();
		}
		catch (InterfridgeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void submitOrder(Order newOrder) throws InterfridgeException {
		currentUser.purchase(localFridge, newOrder.getOrderLines());
	}

	/**
	 * Changes the password of the currently logged-in user.
	 * @param to is the new plaintext password
	 */
	public static void changeCurrentUserPassword(String to) {
		throw new UnsupportedOperationException(); //TODO: How to handle this?
	}

	/**
	 * Adds credit to the current user's account
	 * @param creditAmount is the amount to credit the account by
	 * @throws InterfridgeException 
	 */
	public static void creditCurrentUserAccount(double creditAmount) throws InterfridgeException {
		currentUser.topup(localFridge, (int) (creditAmount * 100));
	}

	/**
	 * Returns a list of the worse balance offenders
	 */
	public static List<String> getWorstOffenders() throws InterfridgeException {
		return localFridge.getNaughtyPeople();
	}

	public static String getSignupInfo() throws InterfridgeException {
		return localFridge.getSignupInfo();
	}

	public static int getNumServed() throws InterfridgeException {
		return localFridge.getNumServed();
	}

	public static List<ProductCategory> getCategories() {
		Set<ProductCategory> categories = new HashSet<ProductCategory>();
		for (StockItem item: cachedStock.values()) {
			categories.add(item.getCategory());
		}
		List<ProductCategory> sortedCategories = new ArrayList<ProductCategory>(categories);
		Collections.sort(sortedCategories);
		System.out.println(sortedCategories);
		return sortedCategories;
	}

	public static List<StockItem> getProductsForCategory(ProductCategory category) {
		List<StockItem> products = new ArrayList<StockItem>();
		for (StockItem item: cachedStock.values()) {
			if (item.getCategory().equals(category)) {
				products.add(item);
			}
		}
		return products;
	}

	public static List<StockItem> getProductsMatchingSearch(String searchString) {
		List<StockItem> products = new ArrayList<StockItem>();
		for (StockItem item: cachedStock.values()) {
			//TODO: Search for multiple words individually
			if (item.description.toLowerCase().contains(searchString.toLowerCase())) {
				products.add(item);
			}
		}
		return products;
	}

	public static boolean checkIfTransactionIsWithinCreditLimit(double totalOrderCost) {
		//TODO: Implement this?
		return true;
	}

	public static boolean validateUserCode(String username) {
		//TODO: Implement this
		//Calling localFridge.getUser(username) would be enough for local users, but we also need to account for remote users
		return true;
	}
}
